package br.ucsal.bes20192.testequalidade.escola.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2003 |	 * 16
	 */
/*Esse teste � falho, aluno nascido em 2003 tera 17 anos*/
	
	@Test
	@DisplayName("Verificar o calculo da idade de aluno nascido em 2003")
	public void testarCalculoIdadeAluno1() {
		
		Aluno aluno = AlunoBuilder	.umAluno()
									.comMatricula(200001072)
									.comNome("Jo�o Silva")
									.comSituacao(SituacaoAluno.ATIVO)
									.comAnoNascimento(2003)
									.build();
		
		AlunoDAO alunoDAO = new AlunoDAO();
		AlunoBO alunoBO = new AlunoBO(alunoDAO, new DateHelper());
		
		alunoBO.atualizar(aluno);
		
		Integer resultadoEsperado = 16;
		Integer resultadoAtual = alunoBO.calcularIdade(aluno.getMatricula());
		
		assertEquals(resultadoEsperado, resultadoAtual);
		
	}

	@Test
	@DisplayName("Verificar se alunos ativos sao atualizados")
	public void testarAtualizacaoAlunosAtivos() {
		
		Aluno aluno = AlunoBuilder	.umAluno()
									.comMatricula(200001073)
									.comNome("Maria Santos")
									.comSituacao(SituacaoAluno.ATIVO)
									.comAnoNascimento(1998)
									.build();
		
		AlunoDAO alunoDAO = new AlunoDAO();
		
		AlunoBO alunoBO = new AlunoBO(alunoDAO, new DateHelper());
		alunoBO.atualizar(aluno);
		
		Aluno resultadoEsperado = aluno;
		Aluno resultadoAtual = alunoDAO.encontrarPorMatricula(aluno.getMatricula());
		
		assertEquals(resultadoEsperado, resultadoAtual);
		
	}
	
	@AfterAll
	@DisplayName("Limpar a tabela de alunos ap�s todos os testes")
	public static void limparTabelaAposTodosOsTestes() {
		AlunoDAO alunoDAO = new AlunoDAO();
		alunoDAO.excluirTodos();
	}

}
