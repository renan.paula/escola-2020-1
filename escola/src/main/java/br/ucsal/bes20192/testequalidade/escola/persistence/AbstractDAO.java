package br.ucsal.bes20192.testequalidade.escola.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AbstractDAO {

	private static final String USER = "root";
	private static final String PASSWORD = "12345";
	private static final String STRING_CONNECTION = "jdbc:mysql://localhost:3306/atividade05?useTimezone=true&serverTimezone=UTC";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	
	private Connection connection = null;

	protected Connection getConnection() {
		if (connection == null) {
			conectar();
		}
		return connection;
	}

	private void conectar() {
		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(STRING_CONNECTION, USER, PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
